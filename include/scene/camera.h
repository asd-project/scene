//---------------------------------------------------------------------------

#pragma once

#ifndef CAMERA_H
#define CAMERA_H

//---------------------------------------------------------------------------

#include <scene/object.h>
#include <gfx3d/uniform.h>

//---------------------------------------------------------------------------

namespace asd
{
	namespace scene
	{
		using namespace gfx;
		
		class container;
		
		enum class projection
		{
			ortho,
			perspective,
			screen,
		};
		
		class camera : public space::oriented
		{
		public:
			api(scene)
			camera(uniform_component & uniforms);
			
			api(scene)
			camera(scene::camera && camera) noexcept;
			
			api(scene)
			camera & operator = (camera &&) noexcept;
			
			api(scene)
			virtual void set_rotation(const quaternion & rot) override;
			
			api(scene)
			virtual void rotate(const quaternion & rot) override;
			
			api(scene)
			virtual void move(const vector & offset) override;
			
			real pitch() const {
				return _angles[0];
			}
			
			real yaw() const {
				return _angles[1];
			}
			
			real roll() const {
				return _angles[2];
			}
			
			api(scene)
			virtual void set_pitch(real value);
			
			api(scene)
			virtual void set_yaw(real value);
			
			api(scene)
			virtual void set_roll(real value);
			
			api(scene)
			virtual void set_angles(real pitch, real yaw, real roll);
			
			api(scene)
			virtual void add_pitch(real value);
			
			api(scene)
			virtual void add_yaw(real value);
			
			api(scene)
			virtual void add_roll(real value);
			
			api(scene)
			virtual void add_angles(real pitch, real yaw, real roll);
			
			api(scene)
			void set_target(const space::position & target);
			
			real view_range() const {
				return _range;
			}
			
			real field_of_view() const {
				return _fov;
			}
			
			const matrix & projection_matrix() const {
				return _projection_matrix;
			}
			
			const matrix & view_matrix() const {
				return _view_matrix;
			}
			
			matrix normal_matrix(const matrix & model) const {
				return (_view_matrix * model).invert();
			}
			
			scene::projection projection() const {
				return _projection;
			}
			
			api(scene)
			void set_viewport(const math::int_size & viewport);
			
			api(scene)
			void set_projection(scene::projection mode);
			
			api(scene)
			void set_view_range(real range);
			api(scene)
			void set_field_of_view(real fov);
			
			api(scene)
			void update_projection();
			api(scene)
			void update_view();
			
			api(scene)
			void bind();
		
		protected:
			real _range = 1.0f;
			real _fov = 90.0f;
			
			space::rotation _pitch;
			space::rotation _yaw;
			space::rotation _roll;
			
			scene::vector  _angles = { { 0.0_x, 0.0_x, 0.0_x } };
			scene::projection _projection = projection::ortho;
			space::matrix _projection_matrix;
			space::matrix _view_matrix;
			
			uniform_block<space::matrix> _projection_data;
			uniform_block<space::matrix> _view_data;
			
			space::real _aspect = 1.0_x;
			math::int_size _viewport = {100, 100};
		};
		
		class camera_object : public scene::object, public camera
		{
		public:
			camera_object(scene::container & scene, gfx::uniform_component & uniforms);
            virtual void update(ticks_t ticks) override;
        };
	}
}

//---------------------------------------------------------------------------
#endif
