//---------------------------------------------------------------------------

#pragma once

#ifndef SCENE_H
#define SCENE_H

//---------------------------------------------------------------------------

#include <chrono>

#include "camera.h"

//---------------------------------------------------------------------------

namespace asd
{
	namespace scene
	{
		using namespace std::chrono_literals;
		
		using color = gfx::colorf;
		using viewport_size = math::int_size;
		
		class drawable;
		class container;
		
		class component
		{
		public:
			component(scene::object & object) : _object(object) {}
			
			scene::object & object() {
				return _object;
			}
			
			const scene::object & object() const {
				return _object;
			}
		
		protected:
			scene::object & _object;
		};
		
		class drawable
		{
			deny_copy(drawable);
		
		public:
			api(scene)
			drawable(container & scene, bool transparent = false);
			virtual ~drawable() {}
			
			bool transparent() const {
				return _transparent;
			}
			
			virtual void render(const viewport_size & viewport, real zoom) const = 0;
		
		protected:
			bool _transparent = false;
		};
		
		class container : private boost::noncopyable
		{
		public:
			api(scene)
			container(flow::context & flow, gfx::uniform_component & uniforms);
			
			api(scene)
			container(container &&) noexcept;
			
			api(scene)
			virtual ~container();
			
			container & operator = (container &&) noexcept;
			
			api(scene)
			const viewport_size & viewport() const;
			
			api(scene)
			void set_viewport(const viewport_size & viewport);
			
			template <class Obj, class ... A, useif<based_on<Obj, object>::value, can_construct<Obj, container &, A...>::value>>
			Obj & append(A &&... args) {
                Obj & object = static_cast<Obj &>(**_objects.emplace(_objects.end(), std::make_unique<Obj>(*this, forward<A>(args)...)));
                
                if constexpr (std::is_base_of<scene::drawable, Obj>::value) {
                    if (object.transparent()) {
                        _transparent.push_back(object);
                    } else {
                        _opaque.push_back(object);
                    }
                }
                
				return object;
			}
			
			api(scene)
			matrix normal_matrix(const matrix & model) const;
			
			api(scene)
			virtual void render() const;
		
		protected:
			api(scene)
			virtual void update(ticks_t ticks);
            
            viewport_size _viewport;
			
			array_list<std::unique_ptr<object>> _objects;
			array_list<std::reference_wrapper<drawable>> _opaque;
			array_list<std::reference_wrapper<drawable>> _transparent;
			flow::tick_timer _timer;
			array_list<std::reference_wrapper<uniform>> _uniforms;
		};
		
		class provider
		{
			map<std::string, container> _scenes;
		};
	}
}

//---------------------------------------------------------------------------
#endif
