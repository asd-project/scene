//---------------------------------------------------------------------------

#pragma once

#ifndef DEFERRED_SHADING_SCENE_H
#define DEFERRED_SHADING_SCENE_H

//---------------------------------------------------------------------------

#include "scene.h"
#include <gfx3d/shader.h>
#include <gfx3d/mesh.h>
#include <gfx3d/render_target.h>
#include <gfx3d/render_pass.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace scene
    {
        class point_light : public scene::object
        {
        public:
            api(scene)
            point_light(scene::container & container, gfx::uniform & lightUniform);

            api(scene)
            void apply() const;

            api(scene)
            virtual void update(ticks_t) override;

            space::vector position = { 0.0_x, 0.0_x, 0.0_x };
            scene::color color = { 1.0_x, 1.0_x, 1.0_x };

            struct
            {
                space::real constant;
                space::real linear;
                space::real exponential;
            } attenuation = { 0.0_x, 0.0_x, 0.0_x };

            space::real ambient_factor = { 1.0_x };

            gfx::uniform_block<gfx::f32v4, scene::color, gfx::f32v4> _point_light_block;
        };

        class ds_container : public container
        {
        public:
            api(scene)
            ds_container(
                const math::uint_size & screen_size,
                flow::context & flow,
                gfx::uniform_component & uniforms,
                gfx::shader_component & shaders,
                gfx::mesh_component & meshes,
                gfx::render_target_component & targets,
                gfx::render_pass_component & passes
            );

            virtual ~ds_container() {}

            api(scene)
            point_light & add_light();

            api(scene)
            void set_global_light_color(const scene::color & color);

            api(scene)
            virtual void render() const override;

        protected:
            // virtual api(scene) void onWidgetResize(handle<WidgetResizeMessage> & msg, Widget & w) override;

            gfx::render_target & _surface;
            gfx::texture & _positions;
            gfx::texture & _normals;
            gfx::texture & _colors;
    
            gfx::render_pass & _main_pass;
            gfx::render_pass & _post_process_pass;
    
            gfx::mesh & _quad;

            gfx::uniform & _point_light_uniform;
            gfx::uniform & _global_light_uniform;
            gfx::uniform & _fog_uniform;

            array_list<point_light> _lights;
            gfx::uniform_block<scene::color> _global_light_color;

            struct
            {
                gfx::shader_program & global;
                gfx::shader_program & point;
            } _light_shaders;
        };
    }
}

//---------------------------------------------------------------------------
#endif
