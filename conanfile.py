import os

from conans import ConanFile, CMake

project_name = "scene"


class SceneConan(ConanFile):
    name = "asd.%s" % project_name
    version = "0.0.1"
    license = "MIT"
    author = "bright-composite"
    url = "https://gitlab.com/asd-project/%s" % project_name
    description = "3D scene components"
    settings = "os", "compiler", "build_type", "arch"
    topics = ("asd", project_name)
    generators = "cmake"
    exports_sources = "include*", "src*", "CMakeLists.txt", "bootstrap.cmake"
    requires = (
        "asd.gfx3d/0.0.1@bright-composite/testing",
        "asd.flow/0.0.1@bright-composite/testing",
        "asd.space/0.0.1@bright-composite/testing",
        "asd.gfx3d_templates/0.0.1@bright-composite/testing"
    )
    
    def source(self):
        pass
    
    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()
    
    def package(self):
        self.copy("*.h", dst="include", src="include")
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)
    
    def package_info(self):
        self.cpp_info.libs = [project_name]
