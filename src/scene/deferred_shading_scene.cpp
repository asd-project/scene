//---------------------------------------------------------------------------

#include <scene/deferred_shading_scene.h>
#include <gfx3d_templates/mesh_quad.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace scene
    {
        point_light::point_light(scene::container & container, gfx::uniform & lightUniform) :
            scene::object(container),
            _point_light_block(lightUniform.create_block<gfx::f32v4, scene::color, gfx::f32v4>())
        {}

        void point_light::update(ticks_t) {
            _point_light_block.set(position, color, float4{ attenuation.constant, attenuation.linear, attenuation.exponential, ambient_factor });
        }

        void point_light::apply() const {
            _point_light_block.bind();
        }

        //void ds_container::onWidgetResize(handle<WidgetResizeMessage> & msg, Widget & w) {
        //    Scene::onWidgetResize(msg, w);
        //    _surface->setSize(widget().size());
        //}

        ds_container::ds_container(
            const math::uint_size & screen_size,
            flow::context & flow,
            gfx::uniform_component & uniforms,
            gfx::shader_component & shaders,
            gfx::mesh_component & meshes,
            gfx::render_target_component & targets,
            gfx::render_pass_component & passes
        ) :
            container(flow, uniforms),
            _surface(targets.create(screen_size)),
            _positions(_surface.add_buffer(gfx::texture_format::rgb16f)),
            _normals(_surface.add_buffer(gfx::texture_format::rgba16f)),
            _colors(_surface.add_buffer(gfx::texture_format::rgba)),
            _main_pass(passes.create({_surface, gfx::colorf(0.0f, 0.0f, 0.0f), 1.0f})),
            _post_process_pass(passes.create({gfx::colorf(1.0f, 0.0f, 0.0f), 1.0f})),
            _quad(gfx::mesh_quad<gfx::vertex_layouts::p2>(meshes)),
            _point_light_uniform(uniforms.find("PointLight")),
            _global_light_uniform(uniforms.find("GlobalLight")),
            _fog_uniform(uniforms.find("Fog")),
            _global_light_color(_global_light_uniform.create_block<scene::color>({ 0.0f, 0.0f, 0.0f })),
            _light_shaders{
                shaders.find("3d/deferred/light/global"),
                shaders.find("3d/deferred/light/point")
            }
        {}

        point_light & ds_container::add_light() {
            return *_lights.emplace(_lights.end(), *this, _point_light_uniform);
        }

        void ds_container::set_global_light_color(const scene::color & color) {
            _global_light_color = color;
        }

        void ds_container::render() const {
            _point_light_uniform.sync();
            _global_light_uniform.sync();
            _fog_uniform.sync();

        //    auto dt = hold(g.depthTestState(), true);
        //    auto clearColor = g.clearColor();
    
            _main_pass.start();
    
            //    g.setClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        //    _surface->clear();	// clear positions, normals and depth
        //    g.setClearColor(clearColor);
        //    _surface->clear(bitmask<2>::value);	// clear colors

            for (auto & drawable : _opaque) {
                drawable.get().render(_viewport, 1.0_x);
            }
    
            _main_pass.end();
            
            _post_process_pass.start();
    
        //    g.setClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        //    space().surface()->clear();
        //    g.setClearColor(clearColor);
            _positions.bind(0);
            _normals.bind(1);
            _colors.bind(2);

            _light_shaders.global.bind();
            _quad.render();

        //    auto am = hold(g.accumulationState(), true);

            _light_shaders.point.bind();

            for (auto & light : _lights) {
                light.apply();
                _quad.render();
            }
        }
    }
}

//---------------------------------------------------------------------------
