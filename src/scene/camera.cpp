//---------------------------------------------------------------------------

#include <scene/camera.h>
#include <scene/scene.h>

//---------------------------------------------------------------------------

namespace asd
{
	namespace scene
	{
		camera::camera(uniform_component & uniforms) :
			_projection_data(uniforms.create_block<space::matrix>("Projection")),
			_view_data(uniforms.create_block<space::matrix>("View"))
		{}
		
		camera::camera(scene::camera && camera) noexcept :
			space::oriented(std::forward<scene::camera>(camera)),
			
			_range(camera._range),
			_fov(camera._fov),
			
			_pitch(std::move(camera._pitch)),
			_yaw(std::move(camera._yaw)),
			_roll(std::move(camera._roll)),
			
			_angles(std::move(camera._angles)),
			_projection(camera._projection),
			_projection_matrix(std::move(camera._projection_matrix)),
			_view_matrix(std::move(camera._view_matrix)),
			
			_projection_data(std::move(camera._projection_data)),
			_view_data(std::move(camera._view_data)),
			
            _aspect(std::move(camera._aspect)),
            _viewport(std::move(camera._viewport))
		{}
		
		scene::camera & camera::operator = (scene::camera && camera) noexcept {
			space::oriented::operator = (std::forward<scene::camera>(camera)),
			
			std::swap(_range, camera._range),
			std::swap(_fov, camera._fov);
		
			std::swap(_pitch, camera._pitch);
			std::swap(_yaw, camera._yaw);
			std::swap(_roll, camera._roll);
			
			std::swap(_angles, camera._angles);
			std::swap(_projection, camera._projection);
			std::swap(_projection_matrix, camera._projection_matrix);
			std::swap(_view_matrix, camera._view_matrix);
			
			std::swap(_projection_data, camera._projection_data);
			std::swap(_view_data, camera._view_data);
            
            std::swap(_aspect, camera._aspect);
            std::swap(_viewport, camera._viewport);
            
			return *this;
		}
		
		void camera::set_viewport(const math::int_size & viewport) {
			_aspect = viewport.ratio<real>();
			_viewport = viewport;
		}
		
		void camera::set_projection(scene::projection mode) {
			_projection = mode;
		}
		
		void camera::update_projection() {
			scene::matrix matrix;
			
			switch (_projection) {
				case projection::ortho: {
					matrix = matrix::ortho(-1.0_x * _aspect, 1.0_x * _aspect, -1.0_x, 1.0_x, 0.001_x, _range);
					break;
				}
				
				case projection::perspective: {
					matrix = matrix::perspective(_fov, _aspect, 0.001_x, 2 * _range);
					break;
				}
				
                case projection::screen: {
					matrix = matrix::ortho(0_x, space::real(_viewport.x), -space::real(_viewport.y), 0_x, 0.001_x, _range);
                    break;
                }
            }
            
            if (_projection_matrix == matrix) {
            	return;
            }
			
			_projection_matrix = matrix;
			_projection_data = _projection_matrix;
		}
		
		void camera::set_view_range(real range) {
			_range = range;
		}
		
		void camera::set_field_of_view(real fov) {
			_fov = fov;
		}
		
		void camera::set_rotation(const quaternion & rot) {
			oriented::set_rotation(rot);
			
			_angles = _rotation.to_euler();
            
            _angles[0] = math::clamp(_angles[0], -math::constants<float>::pi * 0.49_x, math::constants<float>::pi * 0.49_x);
            _angles[1] = math::normalize_angle(_angles[1]);
            
            _pitch = { vector_constants::right, _angles[0] };
			_yaw = { vector_constants::up, _angles[1] };
			_roll = { vector_constants::forward, _angles[2] };
		}
		
		void camera::rotate(const quaternion & rot) {
			_rotation.rotate_by(rot);
		}
		
		void camera::move(const vector & offset) {
			_position += _yaw.apply_to(offset.normalized()) * offset.magnitude();
		}
		
		void camera::set_pitch(real value) {
			_angles[0] = math::clamp(value, -math::constants<float>::half_pi, math::constants<float>::half_pi);
			_pitch = { vector_constants::right, _angles[0] };
			
			oriented::set_rotation(_roll * _yaw * _pitch);
		}
		
		void camera::set_yaw(real value) {
			_angles[1] = math::normalize_angle(value);
			_yaw = { vector_constants::up, _angles[1] };
			
			oriented::set_rotation(_roll * _yaw * _pitch);
		}
		
		void camera::set_roll(real value) {
			_angles[2] = value;
			_roll = { vector_constants::forward, _angles[2] };
			
			oriented::set_rotation(_roll * _yaw * _pitch);
		}
		
		void camera::set_angles(real pitch, real yaw, real roll) {
			_angles[0] = math::clamp(pitch, -math::constants<float>::pi * 0.49_x, math::constants<float>::pi * 0.49_x);
			_angles[1] = math::normalize_angle(yaw);
			_angles[2] = roll;
			
			_pitch = { vector_constants::right, _angles[0] };
			_yaw = { vector_constants::up, _angles[1] };
			_roll = { vector_constants::forward, _angles[2] };
			
			oriented::set_rotation(_roll * _yaw * _pitch);
		}
		
		void camera::add_pitch(real value) {
            set_pitch(_angles[0] + value);
		}
		
		void camera::add_yaw(real value) {
            set_yaw(_angles[1] + value);
		}
		
		void camera::add_roll(real value) {
            set_roll(_angles[2] + value);
		}
		
		void camera::add_angles(real pitch, real yaw, real roll) {
			set_angles(_angles[0] + pitch, _angles[1] + yaw, _angles[2] + roll);
		}
		
		void camera::set_target(const space::position & target) {
			set_direction(_position.direction_to(target));
		}
		
		void camera::update_view() {
			scene::matrix matrix = matrix::look_to(_position, _rotation.forward(), {0, 1, 0});
			
			if (matrix == _view_matrix) {
				return;
			}
			
			_view_matrix = matrix;
			_view_data = _view_matrix;
		}
		
		void camera::bind() {
			_view_data.bind();
			_projection_data.bind();
		}
		
		camera_object::camera_object(scene::container & scene, gfx::uniform_component & uniforms) : scene::object(scene), camera(uniforms) {
		}
        
        void camera_object::update(ticks_t) {
            update_view();
            update_projection();
        }
    }
}

//---------------------------------------------------------------------------
