//---------------------------------------------------------------------------

#include <scene/scene.h>
#include <scene/camera.h>
#include <function/method.h>

//---------------------------------------------------------------------------

namespace asd
{
	namespace scene
	{
		drawable::drawable(container & scene, bool transparent) : _transparent(transparent) {}
		
		container::container(flow::context & flow, gfx::uniform_component & uniforms) : _timer(flow, 10ms, make_delegate(this, update)) {
			_timer.start();
			
			_uniforms << uniforms.find("Color");
			_uniforms << uniforms.find("Model");
			_uniforms << uniforms.find("View");
			_uniforms << uniforms.find("Projection");
		}
		
		container::container(container && scene) noexcept :
			_objects(std::move(scene._objects)),
			_opaque(std::move(scene._opaque)),
			_transparent(std::move(scene._transparent)),
			_timer(std::move(scene._timer)),
			_uniforms(std::move(scene._uniforms))
		{}
		
		container & container::operator = (container && scene) noexcept {
			std::swap(_objects, scene._objects);
			std::swap(_opaque, scene._opaque);
			std::swap(_transparent, scene._transparent);
			std::swap(_timer, scene._timer);
			std::swap(_uniforms, scene._uniforms);
			
			return *this;
		}
		
		container::~container() {}
		
		const viewport_size & container::viewport() const {
			return _viewport;
		}
		
		void container::set_viewport(const viewport_size & viewport) {
			_viewport = viewport;
		}
		
		void container::render() const {
			for (auto & uniform : _uniforms) {
				uniform.get().sync();
			}
			
			for (auto & drawable : _opaque) {
				drawable.get().render(_viewport, 1.0f);
			}
			
			for (auto & drawable : _transparent) {
				drawable.get().render(_viewport, 1.0f);
			}
		}
		
		matrix container::normal_matrix(const matrix & model) const {
			return model.inverse();
		}
		
		void container::update(ticks_t ticks) {
			for (auto & obj : _objects) {
				obj->update(ticks);
			}
		}
	}
}